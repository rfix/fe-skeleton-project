import moment from "moment";
/**
 * 
 * @param {Date} date 
 * @param {String} format 
 * 
 */
 export function getDate(date, format) {
  let result = new Date(date)
  result = moment(result).format(format ? format : 'YYYY-MM-DD')
  return result;
}
