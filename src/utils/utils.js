export const sessionStorage = () => {
  let results = {
    auth: null, session: null, refresh_token: null, role: null,
  }

  const storage = window.sessionStorage
  try {
    if (storage.data_token) {
      const json = JSON.parse(storage.data_token)
      results.auth = json.authentication
      results.session = json.user_session
      results.role = json.role
      results.refresh_token = json.refresh
    }
  } catch (e) {
    results = {
      auth: null, session: null, refresh_token: null, role: null,
    }
  }

  return results
}
