// src/services/AuthService.js

import axios from 'axios'
import store from '@/store/index'

const baseUrl = process.env.VUE_APP_BACKEND_API_URL

function getAccessToken() {
  const accessToken = `Bearer ${store.state.sessionData.accessToken}`
  const { refreshToken } = store.state.sessionData

  return { refreshToken, accessToken }
}

function getHeader() {
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
    'Access-Control-Allow-Credentials': true,
    Authorization: getAccessToken().accessToken,
    UserSessionToken: getAccessToken().refreshToken,
    'IsWeb': 'true'
  }

  return headers
}

function httpMethod() {
  const get = 'GET'
  const post = 'POST'
  const put = 'PUT'
  const del = 'DELETE'

  return {
    get, post, put, delete: del,
  }
}

export default {
  apiService(url, rowVerb, data) {
    // createAuthRefreshInterceptor(axios,refreshAuthLogic);
    const instance = axios.create()
    instance.interceptors.response.use(response => response, error => {
      const data = error.response.data
      if (error.response.status === 401 && error.response.data.errorCode === 'E4011') {
        return axios.post(`${baseUrl}v1/auth/refresh-token`, null, { headers: getHeader() })
          .then(res => {
            store.commit('SESSION_DATA', res.data.data)
            error.response.config.headers.Authorization = getAccessToken().accessToken
            error.response.config.headers.UserSessionToken = getAccessToken().refreshToken
            if (store.state.sessionData.refreshCount === undefined) {
              store.commit('REFRESH_COUNT', 1)
            } else if (store.state.sessionData.refreshCount > 100) {
              return Promise.reject('Refresh limit reached!')
            } else {
              store.commit('REFRESH_COUNT', store.state.sessionData.refreshCount + 1)
            }
          
            const originalRequest = error.config
            const retryOriginalRequest = new Promise((resolve => {
              originalRequest.headers.Authorization = getAccessToken().accessToken
              originalRequest.headers.UserSessionToken = getAccessToken().refreshToken
              resolve(axios(originalRequest))
            }))

            return retryOriginalRequest
          }, () => {
            console.log('Refresh token failed!')
          
            return new Promise((resolve, reject) => {
              store.dispatch('LOG_OUT')
              resolve();
            })
          })
      }

      if (error.response.status === 401 && data.errorCode === 'E4012') {
        return new Promise((resolve) => {
          store.dispatch('LOG_OUT')
          resolve();
        })
      } else if (error.response.status === 400) {
        
        store.dispatch('snackBar', {
          text: data.message,
          color: "error",
          type: data.error,
        })
        return Promise.reject(error)
      } else {
        store.dispatch('snackBar', {
          text: "Something went wrong!",
          color: "dark",
          type: data.message,
        })
        return Promise.reject(error)
      }
    })

    const verb = rowVerb === undefined ? '' : rowVerb
    let result = {}
    if (verb === httpMethod().get) {
      result = instance.get(url, { headers: getHeader() })
    } else if (verb === httpMethod().post) {
      result = instance.post(url, JSON.stringify(data), {
        headers: getHeader(),
      })
    } else if (verb === httpMethod().put) {
      result = instance.put(url, JSON.stringify(data), { headers: getHeader() })
    } else if (verb === httpMethod().delete) {
      result = instance.delete(url, { headers: getHeader() })
    }

    return result.then(response => response.data)
  },

  login(credentials) {
    const url = `${baseUrl}v1/auth/signin`

    return this.apiService(url, httpMethod().post, credentials)
  },

  signUp(data, password) {
    data.password = password
    const url = `${baseUrl}v1/auth/signup`
    return this.apiService(url, httpMethod().post, data)
  },

  deleteDevice(id) {
    const url = `${baseUrl}v1/users/mobile-device/${id}`

    return this.apiService(url, httpMethod().put)
  },

  allDataUser(name, size, page) {
    const url = `${baseUrl}v1/users/find?name=${name}&size=${size}&page=${page}&sort=updatedAt,desc`
    return this.apiService(url, httpMethod().get)
  },

  uploadDataUser(file) {
    const url = `${baseUrl}v1/users/upload`

    return axios.post(url, file, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Credentials': true,
        Authorization: getAccessToken().accessToken,
        UserSessionToken: getAccessToken().refreshToken,
      },
    })
  },

  downloadTemplate() {
    const url = `${baseUrl}v1/users/template`

    return axios.get(url, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Credentials': true,
        Authorization: getAccessToken().accessToken,
        UserSessionToken: getAccessToken().refreshToken,
      },
      responseType: 'blob',
    }).then(res => {
      const FILE = window.URL.createObjectURL(new Blob([res.data], {
        type: 'application/vnd.ms-excel',
      }))
      const docUrl = document.createElement('a')
      docUrl.href = FILE
      docUrl.setAttribute('download', 'template-import-user.xlsx')
      document.body.appendChild(docUrl)
      docUrl.click()
    })
  },

  allDataTajuk(title, size, page) {
    const url = `${baseUrl}v1/app/find?title=${title}&size=${size}&page=${page}`
    return this.apiService(url, httpMethod().get)
  },

  saveTajuk(data, isEdit) {
    const url = `${baseUrl}v1/app/` + (isEdit ? `${data.id}` : 'create')
    return this.apiService(url, isEdit ? httpMethod().put : httpMethod().post, data)
  },

  deleteTajuk(data) {
    const url = `${baseUrl}v1/app/` + data
    return this.apiService(url, httpMethod().delete, null)
  },

  getTajukById(id) {
    const url = `${baseUrl}v1/app/${id}`
    return this.apiService(url, httpMethod().get)
  },

  getWorkingdays(date, size, page) {
    const url = `${baseUrl}v1/working-day?page=${page}&size=${size}&date=${date}`
    return this.apiService(url, httpMethod().get, null)
  },

  updateWorkingDay(users, dates) {
    const data = {
      userIds: users,
      isAll: false,
      listDate: dates
    }
    const url = `${baseUrl}v1/working-day`
    return this.apiService(url, httpMethod().post, data)
  },

  detailWorkingDay(date) {
    const url = `${baseUrl}v1/working-day/details?date=${date}`
    return this.apiService(url, httpMethod().get, null)
  },

  deleteWorkingDay(date) {
    const url = `${baseUrl}v1/working-day?date=${date}`
    return this.apiService(url, httpMethod().delete, null)
  }, 

  updateUser(id, data) {
    const url = `${baseUrl}v1/users/updateUser/${id}`
    return this.apiService(url, httpMethod().put, data)
  },

  getDropdownByFlagging(flagging, search) {
    const url = `${baseUrl}v1/dropdown?flagging=${flagging}&search=${search}`
    return this.apiService(url, httpMethod().get)
  },

  flaggingDropdown() {
    const url = `${baseUrl}v1/dropdown/flagging`
    return this.apiService(url, httpMethod().get)
  },

  saveDropdown(data) {
    const url = `${baseUrl}v1/dropdown`
    return this.apiService(url, httpMethod().post, data)
  },

  deleteDropdown(id) {
    const url = `${baseUrl}v1/dropdown/${id}`
    return this.apiService(url, httpMethod().delete)
  },

  allDataPick(size, page) {
    const url = `${baseUrl}v1/working-day?size=${size}&page=${page}`
    return this.apiService(url, httpMethod().get)
  },

  getUserProfile(){
    const url = `${baseUrl}v1/users`
    return this.apiService(url, httpMethod().get)
  },

  listPresence(data) {
    const url = `${baseUrl}v1/presence?startAt=${data.startAt}&endAt=${data.endAt}&page=${data.page}&size=${data.size}&sort=loginAt,desc&sort=logoutAt,desc&name=${data.name}`
    return this.apiService(url, httpMethod().get)
  },

  updatePersence(data) {
    const url = `${baseUrl}v1/presence`
    return this.apiService(url, httpMethod().put, data)
  },

  getDataPresence(date, flagging) {
    const url = `${baseUrl}v1/presence/count-penalty?date=${date}&flagging=${flagging}`
    return this.apiService(url, httpMethod().get)
  },

  getDataPresenceYearly() {
    const url = `${baseUrl}v1/presence/count-penalty-year`
    return this.apiService(url, httpMethod().get)
  },

  getHistoryPresenceDaily(date, userId) {
    const url = `${baseUrl}v1/presence/penalty-total-daily?date=${date}&userId=${userId}`
    return this.apiService(url, httpMethod().get)
  },
    
  downloadReport(startAt, endAt) {
    const url = `${baseUrl}v1/users/download?startAt=${startAt}&endAt=${endAt}`

    return axios.get(url, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Credentials': true,
        Authorization: getAccessToken().accessToken,
        UserSessionToken: getAccessToken().refreshToken,
      },
      responseType: 'blob',
    }).then(res => {
      const FILE = window.URL.createObjectURL(new Blob([res.data], {
        type: 'application/vnd.ms-excel',
      }))
      const docUrl = document.createElement('a')
      docUrl.href = FILE
      docUrl.setAttribute('download', `report_presence_${startAt}_${endAt}.xlsx`)
      document.body.appendChild(docUrl)
      docUrl.click()
    })
  },

  downloadReportByUser(periode, user, userId) {
    const url = `${baseUrl}v1/presence/export-byuser?periode=${periode}&userId=${userId}`

    return axios.get(url, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        'Access-Control-Allow-Credentials': true,
        Authorization: getAccessToken().accessToken,
        UserSessionToken: getAccessToken().refreshToken,
      },
      responseType: 'blob',
    }).then(res => {
      const FILE = window.URL.createObjectURL(new Blob([res.data], {
        type: 'application/vnd.ms-excel',
      }))
      const docUrl = document.createElement('a')
      docUrl.href = FILE
      docUrl.setAttribute('download', `report_presence_${user}_${periode}.xlsx`)
      document.body.appendChild(docUrl)
      docUrl.click()
    })
  },

  runScheduler(date) {
    const url = `${baseUrl}scheduler/run?value=auto_logout&date=${date}`
    return this.apiService(url, httpMethod().get)
  }

}

