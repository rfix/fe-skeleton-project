// src/store.js (Vue CLI 1.x & 2.x) oder src/store/index.js (Vue CLI 3.x or newer)

import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const getDefaultState = () => ({
  accessToken: '',
  user: {},
  refreshToken: '',
})

export default new Vuex.Store({
  // strict: true,
  plugins: [createPersistedState()],
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => state.accessToken,
    getUser: state => state.user,
  },
  mutations: {
    SET_TOKEN: (state, accessToken) => {
      state.accessToken = accessToken
    },
    SET_USER: (state, user) => {
      state.user = user
      state.accessToken = user.accessToken
      state.refreshToken = user.refreshToken

      // set auth header
      console.log(user)
      Axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`
      Axios.defaults.headers.common.UserSessionToken = `${refreshToken}`
    },
    SET_REFRESH_TOKEN: (state, refreshToken) => {
      state.refreshToken = refreshToken
    },
    RESET: state => {
      Object.assign(state, getDefaultState())
    },
  },
  actions: {
    async login({ commit }, user) {
      console.log('store')
      console.log(user)
      commit('SET_TOKEN', user.accessToken)
      commit('SET_REFRESH_TOKEN', user.refreshToken)

      // set auth header
      Axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`
      Axios.defaults.headers.common.UserSessionToken = `${refreshToken}`
    },
    logout: ({ commit }) => {
      commit('RESET', '')
    },
  },
})
