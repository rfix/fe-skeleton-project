import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function activationGuard(to, from, next) {
  const dataStorage = localStorage.getItem('vuex') != null
  if (dataStorage) {
    next()
  } else {
    next('/pages/login')
  }
}

const routes = [
  {
    path: '/',
    redirect: 'dashboard',
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    beforeEnter: activationGuard,
    component: () => import('@/views/dashboard/Dashboard.vue'),
  },
  {
    path: '/userList',
    name: 'userList',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/userList/UserList.vue'),
  },
  {
    path: '/pickList',
    name: 'picList',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/pickList/WorkingDay.vue'),
  },
  {
    path: '/tajukList',
    name: 'tajukList',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/tajuk/TajukList.vue'),
  },
  {
    path: '/dropdown',
    name: 'dropdown',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/dropdown/Dropdown.vue'),
  },
  {
    path: '/presence',
    name: 'presence',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/userList/Presence.vue'),
  },
  {
    path: '/typography',
    name: 'typography',
    beforeEnter: activationGuard,
    component: () => import('@/views/typography/Typography.vue'),
  },
  {
    path: '/icons',
    name: 'icons',
    beforeEnter: activationGuard,
    component: () => import('@/views/icons/Icons.vue'),
  },
  {
    path: '/cards',
    name: 'cards',
    beforeEnter: activationGuard,
    component: () => import('@/views/cards/Card.vue'),
  },
  {
    path: '/simple-table',
    name: 'simple-table',
    beforeEnter: activationGuard,
    component: () => import('@/views/simple-table/SimpleTable.vue'),
  },
  {
    path: '/form-layouts',
    name: 'form-layouts',
    beforeEnter: activationGuard,
    component: () => import('@/views/form-layouts/FormLayouts.vue'),
  },
  {
    path: '/pages/account-settings',
    name: 'pages-account-settings',
    beforeEnter: activationGuard,
    component: () => import('@/views/pages/account-settings/AccountSettings.vue'),
  },
  {
    path: '/pages/login',
    name: 'pages-login',
    component: () => import('@/views/pages/Login.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '/pages/register',
    name: 'pages-register',
    component: () => import('@/views/pages/Register.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '/error-404',
    name: 'error-404',
    component: () => import('@/views/Error.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '*',
    redirect: 'error-404',
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
