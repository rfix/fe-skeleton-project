import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '@/router/index'

Vue.use(Vuex)

export default new Vuex.Store({
  // strict: true,
  namespaced: true,
  plugins: [createPersistedState()],
  state: {
    snackbar: { 
      show: false,
      color: "",
      text: "",
    },
    sessionData: {
      accessToken: null,
      refreshToken: null,
      refreshCount: 0,
      username: null,
    },
  },
  getters: {
    isLoggedIn: state => state.accessToken,
    getUser: state => state.user,
  },
  mutations: {
    SHOW_MESSAGE(state, payload) {
      state.snackbar = payload;
    },
    SESSION_DATA: (state, data) => {
      state.sessionData.accessToken = data.accessToken
      state.sessionData.refreshToken = data.refreshToken
      state.sessionData.username = data.username
      state.sessionData.refreshCount = 0
    },
    REFRESH_COUNT(state, refreshCounter) {
      state.sessionData.refreshCount += refreshCounter
    },
    LOG_OUT_MUTATION(state) {
      state.sessionData.accessToken = null
      state.sessionData.refreshToken = null
      state.sessionData.refreshCount = 0
    }
  },
  actions: {
    async login({ commit }, user) {
      commit('SET_TOKEN', user.accessToken)
      commit('SET_REFRESH_TOKEN', user.refreshToken)

      // set auth header
      Axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`
      Axios.defaults.headers.common.UserSessionToken = `${refreshToken}`
    },
    LOG_OUT({commit}){
      commit('LOG_OUT_MUTATION');
  
      router.push({
        name:'pages-login'
      })
    },
    snackBar({ commit }, payload) {
      if (payload.type === 'success') {
        payload = {
          text: "Successfully Saved!",
          color: "success",
          type: "success",
        }
      } else if (payload.type === 'error') {
        payload = {
          text: "Something Where Wrong!",
          color: "error",
          type: "error",
        }
      }
      commit("SHOW_MESSAGE", payload);
    },
  },
})
